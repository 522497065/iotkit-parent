package cc.iotkit.contribution.data;

import cc.iotkit.data.ICommonData;
import cc.iotkit.contribution.model.IotContributor;
import cc.iotkit.data.ICommonData;

/**
 * 数据接口
 *
 * @author Lion Li
 * @date 2023-07-09
 */
public interface IIotContributorData extends ICommonData<IotContributor, Long> {


}
